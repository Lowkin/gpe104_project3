﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public static GameManager gm; // Singleton GameManager object

    private int currentScore = 0; // players current score

    public float cameraFollowSpeed; // Follow speed for camera
    public float countToWin; // how many collectibles are needed to win
    public Camera mainCamera; // reference for our camera
    public GameObject playerSpawnPoint; // player spawn location
    public GameObject mainMenuUI; // Variable to store a reference to our main menu 
    public GameObject gameOverUI; // Variable to store a reference to our game over menu
    public GameObject winUI; // Variable to store a reference to our win menu
    public GameObject gamePlayUI; // Variable to store a reference to our game play ui
    public Controller playerController; // Variable to hold a reference to our player controller
    public GameObject malePawn; // Variable to hold a reference to our male pawn
    public GameObject femalePawn; // Variable to hold a reference to our female pawn
    public Toggle maleOption; // Variable to hold a reference to our male gender selection
    public Toggle femaleOption; // Variable to hold a reference to our female gender selection
    public Text score; // Variable to hold a reference to our score UI

    // declarations for our various layers
    [HideInInspector]
    public int playerLayer;
    [HideInInspector]
    public int enemyLayer;
    [HideInInspector]
    public int collectibleLayer;
    [HideInInspector]
    public int groundLayer;
    [HideInInspector]
    public int loudGroundLayer;
    [HideInInspector]
    public int obstacleLayer;

    [HideInInspector]
    public enum States { Menu, Active, GameOver }; // Variable to hold the various states our game
    [HideInInspector]
    public States currentState; // Variable to hold the current state of our pawn
    [HideInInspector]
    public bool gameOver = false; // trigger to end the game
    public GameObject playerPawn; // reference to our player's current pawn

    void Awake()
    {
        // If we don't have a GameManager instance set our singleton to this instance
        if (gm == null)
            gm = this;
        // Otherwise delete this one
        else
            Destroy(this.gameObject);

        // Assign our layer variables
        playerLayer = LayerMask.NameToLayer("Player");
        enemyLayer = LayerMask.NameToLayer("Enemy");
        collectibleLayer = LayerMask.NameToLayer("Collectible");
        groundLayer = LayerMask.NameToLayer("Ground");
        loudGroundLayer = LayerMask.NameToLayer("LoudGround");
        obstacleLayer = LayerMask.NameToLayer("Obstacles");

        score.text = currentScore + " / " + countToWin; // set our gameplay UI values

        currentState = States.Menu;
    }

    // Use this for initialization
    void Start () {
        StartCoroutine("GameFSM"); // start our game state machine
	}
	
    // FSM to run our game 
    public IEnumerator GameFSM()
    {
        // we always want to be running this
        while (true)
        {
            // if we are in the main menu
            if (currentState == States.Menu)
            {
                mainMenuUI.SetActive(true); // set our main menu UI active
                gameOverUI.SetActive(false); // set our game over UI inactive
                gamePlayUI.SetActive(false); // set our gameplay UI inactive
                winUI.SetActive(false); // set our win ui inactive
                yield return null;
            }
            // if we are in an active state
            else if (currentState == States.Active)
            {
                mainMenuUI.SetActive(false); // set our main menu UI inactive
                gameOverUI.SetActive(false); // set our game over UI inactive
                gamePlayUI.SetActive(true); // set our gameplay UI active
                winUI.SetActive(false); // set our win ui inactive

                // if we have toggled to game over
                if (gameOver)
                    currentState = States.GameOver; // if something triggered game over set our state

                // otherwise  the camera needs to follow the player
                else
                {
                    Vector3 cameraDirection = new Vector3(playerPawn.transform.position.x, playerPawn.transform.position.y, mainCamera.transform.position.z); // get our desired location for the camera
                    mainCamera.transform.position = Vector3.Lerp(mainCamera.transform.position, cameraDirection, Time.deltaTime * cameraFollowSpeed); // smoothly move the camera to the desired location
                }
                yield return null;
            }
            // if we are in a game over state
            else if (currentState == States.GameOver)
            {
                gameOver = false;
                mainMenuUI.SetActive(false); // set our main menu UI inactive
                gamePlayUI.SetActive(false); // set our gameplay UI inactive

                // if we got enough score we won, otherwise we lost
                if (currentScore == countToWin)
                {
                    winUI.SetActive(true); // set our win ui active
                    gameOverUI.SetActive(false); // set our game over UI inactive
                }
                else
                {
                    winUI.SetActive(false); // set our win ui inactive
                    gameOverUI.SetActive(true); // set our game over UI active
                }

                yield return null;
            }
            yield return null; // make sure the rest of our game can run its logic
        }
    }
    public void Collect()
    {
        currentScore++; // increment our score
        score.text = currentScore + " / " + countToWin; // update our UI to display our score

        // if we just collected all the required loot we win
        if (currentScore == countToWin)
            gameOver = true;
    }

    // function to start the game
    public void Play()
    {
        EnemyController[] enemies = Component.FindObjectsOfType<EnemyController>();

        // only create a single player pawn
        if (playerPawn == null)
        {
            // if we have selected male, instantiate a male pawn
            if (maleOption.isOn)
                playerPawn = Instantiate(malePawn, playerSpawnPoint.transform.position, Quaternion.identity, playerController.transform);
            //if we have selected female, instantiate a female pawn
            else
                playerPawn = Instantiate(femalePawn, playerSpawnPoint.transform.position, Quaternion.identity, playerController.transform);
        }

        // set our gamestate
        currentState = States.Active;

        // loop thorugh all our enemies and start their AI state machines
        for (int i = 0; i < enemies.Length; i++)
        {
            enemies[i].StartCoroutine("EnemyAIFSM");
        }
    }

    // function to handle quitting the application
    public void Quit()
    {
        Application.Quit(); // close the application
    }

    // function to return to main menu
    public void MainMenu()
    {
        SceneManager.LoadScene(0); // reload our game scene
    }
}
