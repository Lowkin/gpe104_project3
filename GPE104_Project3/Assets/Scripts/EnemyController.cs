﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : Controller {
    
    private enum States { Idle, Patrol, Looking}; // Variable to hold the various states our pawn can have
    private States currentState; // Variable to hold the current state of our pawn
    private int currentDestination; // Variable to hold the current waypoint we are moving to
    private int direction = 1; // Variable to indicate if we are moving forward or backwards through our waypoints
    private EnemyPawn enemyPawn; // EnemyPawn variable to hold reference to our current pawn

    public GameObject[] wayPoints; // List of empty gameobjects used for patrolling enemy waypoints
    public bool loop = false; // Variable to indicate if the AI should loop its route or go from one end to the other
    public float continueDistance; // Variable to hold how close the pawn needs to be to the current destination before continuing on
    public float maxWaitTime = 0; // Variable to hold how long we will wait between patrol points

    // Use this for initialization
    public override void Start () {
        base.Start();
        currentDestination = 0; // Start by going to the first patrol point
        currentState = States.Patrol; // set our starting state to patrol
        enemyPawn = currentPawn as EnemyPawn; // set our current pawn reference
        StartCoroutine("EnemyAIFSM"); // start our enemy AI state machine
    }
	
	// Update is called once per frame
	void Update () {

	}

    // FSM to control how our enemy AI works
    private IEnumerator EnemyAIFSM() 
    {
        while (gm.currentState == GameManager.States.Active)
        {
            // If our current state is idle we sit idly by unless we hear something
            if (currentState == States.Idle)
            {
                enemyPawn.isMoving = false; // set our pawn to a non moving state
                float wait = 0; // current amount of time paused
                float waitTime = Random.Range(0, maxWaitTime); // max amount of time paused

                enemyPawn.uiQuestionMark.SetActive(true); // set our question mark UI to active
                enemyPawn.uiExclamation.SetActive(false); // set our exclamation point to inactive
                enemyPawn.uiAttention.gameObject.SetActive(false); // set our attention bar inactive

                // while we have been waiting less time than the wait time
                while (wait < waitTime)
                {
                    if (enemyPawn.noiseDetected)
                    {
                        enemyPawn.noiseDetected = !enemyPawn.noiseDetected; // set our trigger back
                        wait = waitTime; // set our wait to waitTime to break our pause
                        currentState = States.Looking; // set our state to looking
                    }
                    wait += Time.deltaTime; // increment wait time
                    yield return null;
                }
                if (currentState != States.Looking) // if we are still idle get back to work
                    currentState = States.Patrol; // set state back to patrol
            }
            // if our current state is patrol we patrol between our specified waypoints
            else if (currentState == States.Patrol)
            {
                enemyPawn.isMoving = true; // set our pawn to a moving state

                enemyPawn.uiQuestionMark.SetActive(true); // set our question mark UI to active
                enemyPawn.uiExclamation.SetActive(false); // set our exclamation point to inactive
                enemyPawn.uiAttention.gameObject.SetActive(false); // set our attention bar inactive

                // Check if we heard something
                if (enemyPawn.noiseDetected)
                {
                    currentState = States.Looking; // if we heard something we switch to the looking state
                }
                // if we do not have any waypoints (i.e. we are just a stationary guard
                else if (wayPoints.Length == 0)
                {
                    yield return null;
                }
                // We check to see if we need to pick a new patrol point and direction
                else if (Vector3.Distance(enemyPawn.gameObject.transform.position, wayPoints[currentDestination].transform.position) <= continueDistance)
                {
                    if (!loop)
                    {
                        // Check if going in the current direction is still possible 
                        if (direction + currentDestination < 0 || direction + currentDestination >= wayPoints.Length)
                            direction = -direction; // Flip our direction

                        // update our destination in the right direction
                        currentDestination += direction; // increment our index in our waypoint array
                    }
                    else
                    {
                        currentDestination = currentDestination == wayPoints.Length - 1 ? 0 : currentDestination + 1;
                    }
                    currentState = States.Idle; // swap to the idle state for a random amount of time before resuming patrol
                    yield return null;
                }
                // Otherwise we keep moving in the current direction
                else
                    enemyPawn.Move((wayPoints[currentDestination].transform.position - enemyPawn.gameObject.transform.position).normalized);

                yield return null;
            }
            // if our current state is looking we've heard something so we are rotating to look in that direction
            else if (currentState == States.Looking)
            {
                enemyPawn.uiQuestionMark.SetActive(false); // set our question mark UI to inactive
                enemyPawn.uiExclamation.SetActive(true); // set our exclamation point UI to active so player knows the enemy is looking
                enemyPawn.uiAttention.gameObject.SetActive(true); // set our attention bar active

                enemyPawn.isMoving = false; // set our pawn to a non moving state
                float wait = 0; // current amount of time paused

                // while we have been waiting less time than the wait time
                while (wait < enemyPawn.seekTime)
                {
                    wait += Time.deltaTime; // increment wait time
                    enemyPawn.uiAttention.fillAmount = wait / enemyPawn.seekTime; // update our UI fill to show how long the player has to move
                    yield return null;
                }

                enemyPawn.OrientDirection((enemyPawn.noiseLocation - enemyPawn.transform.position).normalized); // look in the direction we heard the noise
                wait = 0; // set our wait back to 0
                
                // look in the direction of the sound for specified time
                while (wait < enemyPawn.lookTime)
                {
                    wait += Time.deltaTime; // increment wait time
                    yield return null;
                }
                enemyPawn.noiseDetected = !enemyPawn.noiseDetected; // set our trigger back
                enemyPawn.uiAttention.fillAmount = 0;
                currentState = States.Patrol; // set state back to patrol
            }
            yield return null;
        }
    }
}
