﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class EnemyPawn : Pawn {

    public float viewAngle = 90f; // how wide of a viewing angle can the unit detect players
    public float seekTime = 2f; // how long does the unit pause before looking in the direction that they heard something in
    public float lookTime = 2f; // how long does the unit look where it heard a noise

    public GameObject uiExclamation; // Variable to hold our exlamation point reference
    public GameObject uiQuestionMark; // Variable to hold our question mark reference
    public Image uiAttention; // Variable to hold our attention bar reference

    private LineRenderer lr;

    [HideInInspector]
    public bool noiseDetected = false; // Variable to trigger if we heard a noise
    [HideInInspector]
    public Vector3 noiseLocation; // Location the noise was heard from

    public override void Start()
    {
        base.Start();
        lr = GetComponent<LineRenderer>() as LineRenderer; // reference to lr
        lr.widthMultiplier = .05f; // thin out our line

        float deltaTheta = (2f * Mathf.PI) / 40; // how much we rotate between each point on the circle
        float theta = 0f; //starting point

        lr.positionCount = 40; // set our line renderer to have 40 positions

        // loop through and at each angle set a position for our line renderer and increment the next angle
        for (int i = 0; i < lr.positionCount; i++)
        {
            Vector3 pos = new Vector3(1 * Mathf.Cos(theta), 1 * Mathf.Sin(theta), 0f);
            lr.SetPosition(i, pos);
            theta += deltaTheta;
        }
    }
}
