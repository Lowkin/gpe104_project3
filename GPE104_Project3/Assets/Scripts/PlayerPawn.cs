﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPawn : Pawn {
    public float soundMultiplier = 1; // Variable to hold how much more sound we do if we are walking over 'loud' ground
    public float soundLevel; // float to control this pawns sound level

    [HideInInspector]
    public float currentSoundLevel; // Variable to hold the current level of our sound

    // Use this for initialization
    public override void Start () {
        base.Start();
        currentSoundLevel = soundLevel; // set current sound to normal sound
        gm.playerController.currentPawn = this; // get a reference to our controller when we are instantiated
	}
	
    private void OnTriggerEnter2D(Collider2D collision)
    {
        // If we are on 'loud' ground up our noise generation
        if (collision.gameObject.layer == gm.loudGroundLayer)
            currentSoundLevel = soundLevel * soundMultiplier;

    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        // If we are on normal ground return our sound level
        if (collision.gameObject.layer == gm.loudGroundLayer)
            currentSoundLevel = soundLevel;
    }
}
