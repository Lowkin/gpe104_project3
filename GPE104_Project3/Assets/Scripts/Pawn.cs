﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Pawn : MonoBehaviour {

    public float pawnSpeed;  // float to control this pawns speed

    public SpriteRenderer sr; // our sprite renderer reference

    public Animator animator; // animator reference

    [HideInInspector]
    public GameManager gm; // gm reference

    [HideInInspector]
    public bool isMoving = false; // bool to control if we are displaying idle or animation

    [HideInInspector]
    public Vector3 currentDirection; // Value to hold the direction we are currently facing

    // Use this for initialization
    public virtual void Start () {
        gm = GameObject.Find("GameManager").GetComponent<GameManager>() as GameManager; // get a reference to our game manager
        currentDirection = transform.right; // our current direction to start with is the way the sprite is facing
	}
	
	// Update is called once per frame
	public virtual void Update () {
        // if we arent moving we will want to set our movement triggers to off
        if (!isMoving)
        {
            ResetTriggers(); // reset our movement triggers
        }
    }

    // Base function to move this pawn in a given direction at its set speed
    public virtual void Move(Vector3 direction)
    {
        currentDirection = direction; // set our pawns current direction to the direction we are wanting to face
        transform.position += (currentDirection * pawnSpeed * Time.deltaTime); // move our pawn in the provided direction
        OrientDirection(currentDirection); // point in the new direction
    }

    // function to point our sprite in the direction we are moving
    public virtual void OrientDirection(Vector3 direction)
    {
        currentDirection = direction; // set our pawns current direction to the direction we are wanting to face
        ResetTriggers(); // set our movement direction triggers back to off
        ResetDirectionTriggers(); // set our facing direction triggers back to off

        // if we are pointing left or right
        if (Mathf.Abs(currentDirection.x) > Mathf.Abs(currentDirection.y))
        {
            // we are going right so set our sprite right
            if (currentDirection.x > 0)
            {
                animator.SetBool("movingRight", true);
                animator.SetBool("facingRight", true);

            }
            // we are going left so set our sprite left
            else
            {
                animator.SetBool("movingLeft", true);
                animator.SetBool("facingLeft", true);

            }
        }
        else
        {
            // we are going up so set our sprite up
            if (currentDirection.y > 0)
            {
                animator.SetBool("movingUp", true);
                animator.SetBool("facingUp", true);

            }
            // we are going down so set our sprite down
            else
            {
                animator.SetBool("movingDown", true);
                animator.SetBool("facingDown", true);

            }
        }
    }

    // function to reset our animation movement triggers to false
    private void ResetTriggers()
    {
        animator.SetBool("movingRight", false);
        animator.SetBool("movingLeft", false);
        animator.SetBool("movingUp", false);
        animator.SetBool("movingDown", false);

    }

    // function to reset our animation direction triggers to false
    private void ResetDirectionTriggers()
    {
        animator.SetBool("facingRight", false);
        animator.SetBool("facingLeft", false);
        animator.SetBool("facingUp", false);
        animator.SetBool("facingDown", false);
    }
}
