﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : Controller {

    private bool moving; // Variable to tell if the player controller is moving

    // Use this for initialization
    public override void Start () {
        base.Start();
        moving = false;
	}
	
	// Update is called once per frame
	void Update () {

        // if we are in an active game state
        if (gm.currentState == GameManager.States.Active && currentPawn != null)
        {
            moving = false;

            // Check W and Up Arrow to move in the positive Y direction
            if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
            {
                currentPawn.Move(transform.up); // Move our pawn up
                moving = true; // set moving bool to true
            }
            // Check S and Down Arrow to move in the negative Y direction
            else if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
            {
                currentPawn.Move(-transform.up); // Move our pawn down
                moving = true; // set moving bool to true
            }
            // Check A and Left Arrow to rotate to the left
            if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
            {
                currentPawn.Move(-transform.right); // Move our pawn left
                moving = true; // set moving bool to true
            }
            // Check D and Right Arrow to rotate to the right
            else if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
            {
                currentPawn.Move(transform.right); // Move our pawn right
                moving = true; // set moving bool to true
            }

            // If we are moving this frame fire a noise
            if (moving)
                FireNoise();
            currentPawn.isMoving = moving;
        }
    }

    // function to make sure we make noise when we are moving
    private void FireNoise()
    {
        // Get all enemies within the current sound level range of our pawn
        Collider2D[] nearbyEnemies = Physics2D.OverlapCircleAll(new Vector2(currentPawn.transform.position.x, currentPawn.transform.position.y), ((PlayerPawn)currentPawn).currentSoundLevel, 1 << gm.enemyLayer);

        // loop through all the enemies and tell them they are in range of a sound our player made
        for (int i = 0; i < nearbyEnemies.Length; i++)
        {
            EnemyListener currentEnemy = nearbyEnemies[i].gameObject.GetComponent<EnemyListener>() as EnemyListener;
            if (currentEnemy != null)
                currentEnemy.Listen(currentPawn.transform, ((PlayerPawn)currentPawn).soundLevel); // call enemy listen function
        }

    }
}
