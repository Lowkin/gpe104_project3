﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour {

    [HideInInspector]
    public GameManager gm; // gm reference
	// Use this for initialization
	void Start () {
        gm = GameObject.Find("GameManager").GetComponent<GameManager>() as GameManager;
    }
	
    public void OnTriggerEnter2D(Collider2D collision)
    {
        // If we were hit by a player
        if (collision.gameObject.layer == gm.playerLayer)
        {
            gm.Collect(); // let the Game Manager know we were collected
            Destroy(this.gameObject); // Destroy ourselves
        }
    }
}
