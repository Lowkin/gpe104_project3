﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWatcher : MonoBehaviour {

    private EnemyPawn enemyPawn; // enemy pawn object

    public LayerMask layerMask;

	// Use this for initialization
	void Start () {
        enemyPawn = gameObject.GetComponent<EnemyPawn>() as EnemyPawn; // get a reference to our parent
	}
	
    // function that receives collision information if a collider runs into our vision collider
    private void OnTriggerStay2D(Collider2D collision)
    {
        // if it is the player that has entered our collider
        if (collision.gameObject.layer == enemyPawn.gm.playerLayer)
        {
            Vector2 location = new Vector2(transform.position.x, transform.position.y); // watcher location
            Vector2 playerLocation = new Vector2(collision.gameObject.transform.position.x, collision.gameObject.transform.position.y); // player pawn location
            RaycastHit2D collisionInfo = Physics2D.Raycast(location, playerLocation - location, 5, layerMask); // fire a ray to the player location

             // If we hit a collider and that collider is a player (i.e. we didnt hit a wall that would block vision) we know we have seen the player
            if (collisionInfo.transform != null && collisionInfo.transform.gameObject.layer == enemyPawn.gm.playerLayer)
            {
                if (Vector3.Angle(collision.transform.position - transform.position, enemyPawn.currentDirection) < enemyPawn.viewAngle / 2) // if the player is within our view angle
                    enemyPawn.gm.gameOver = true; // we found our player game over man
            }
        }
        
    }
}
