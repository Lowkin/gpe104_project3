﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyListener : MonoBehaviour {

    private EnemyPawn enemyPawn; // enemy pawn object
	// Use this for initialization
	void Start () {
        enemyPawn = gameObject.GetComponent<EnemyPawn>() as EnemyPawn;
	}
	 
    // function to make the enemy listen for a sound
    public void Listen(Transform location, float distance)
    {
        // if the sound was close enough to hear
        if (Vector3.Distance(location.position, transform.position) < distance)
        {
            // We heard something so set our noise detection variables
            enemyPawn.noiseDetected = true;
            enemyPawn.noiseLocation = location.position;
        }
    }
}
